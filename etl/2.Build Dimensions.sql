
with insert_locations as 
(
--select unique locations that aren't already in the dimention table
	select lat	     
	,lon
	from staging.AgaricusLepiota s
	where not exists (select 1 from Dim_location as d where s.lat = d.lat and s.lon = d.lon)
	group by lat	     
	,lon
)
insert into Dim_location
(
lat	     
,lon	 
,location
)
select lat	     
,lon
,geography::Point(lat, lon, 4326)  
from insert_locations;


with reference as 
(	
	select 'brown' as name,'n' as [key] 
	union all select 'buff' as name,'b' as [key] 
	union all select 'cinnamon' as name,'c' as [key] 
	union all select 'gray' as name,'g' as [key] 
	union all select 'green' as name,'r' as [key] 
	union all select ' pink' as name,'p' as [key] 
	union all select 'purple' as name,'u' as [key] 
	union all select 'red' as name,'e' as [key] 
	union all select 'white' as name,'w' as [key] 
	union all select 'yellow' as name,'y'as [key]
)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[3] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[3] not in (select [key] from reference)
	)

insert into Dim_cap_color
             (
                          cap_color_Key  
                        , cap_color_name 
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_cap_color as d where stub_records.[key] = d.cap_color_Key);



with reference as 
(	
	select 'bell' as name,'b' as [key] 
	union all select 'conical' as name,'c' as [key] 
	union all select 'convex' as name,'x' as [key] 
	union all select 'flat' as name,'f' as [key] 
	union all select ' knobbed' as name,'k' as [key] 
	union all select 'sunken' as name,'s'   as [key]
)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[1] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[1] not in (select [key] from reference)
	)

insert into Dim_cap_shape
             (
                          cap_shape_Key 
                        , cap_shape_name
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_cap_shape as d where stub_records.[key] = d.cap_shape_Key);




with reference as 
(	
	select 'black' as name,'k' as [key] 
	union all select 'brown' as name,'n' as [key] 
	union all select 'buff' as name,'b' as [key] 
	union all select 'chocolate' as name,'h' as [key] 
	union all select 'gray' as name,'g' as [key] 
	union all select ' green' as name,'r' as [key] 
	union all select 'orange' as name,'o' as [key] 
	union all select 'pink' as name,'p' as [key] 
	union all select 'purple' as name,'u' as [key] 
	union all select 'red' as name,'e' as [key] 
	union all select ' white' as name,'w' as [key] 
	union all select 'yellow' as name,'y' as [key]
)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[9] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[9] not in (select [key] from reference)
	)

insert into Dim_gill_color
             (
                          gill_color_Key  
                        , gill_color_name 
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_gill_color as d where stub_records.[key] = d.gill_color_Key);



with reference as 
(	
	select 'broad' as name,'b' as [key] 
	union all select 'narrow' as name,'n' as [key]
	)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[8] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[8] not in (select [key] from reference)
	)

insert into Dim_gill_size
             (
                          gill_size_Key  
                        , gill_size_name 
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_gill_size as d where stub_records.[key] = d.gill_size_Key);


with reference as 
(	
	select 'grasses' as name,'g' as [key] 
	union all select 'leaves' as name,'l' as [key] 
	union all select 'meadows' as name,'m' as [key] 
	union all select 'paths' as name,'p' as [key] 
	union all select ' urban' as name,'u' as [key] 
	union all select 'waste' as name,'w' as [key] 
	union all select 'woods' as name,'d' as [key]
	)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[22] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[22] not in (select [key] from reference)
	)
insert into Dim_habitat
             (
                           habitat_Key  
                        , habitat_name 
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_habitat as d where stub_records.[key] = d.habitat_Key);

with reference as 
(	
	select 'almond' as name,'a' as [key] 
	union all select 'anise' as name,'l' as [key] 
	union all select 'creosote' as name,'c' as [key] 
	union all select 'fishy' as name,'y' as [key] 
	union all select 'foul' as name,'f' as [key] 
	union all select ' musty' as name,'m' as [key] 
	union all select 'none' as name,'n' as [key] 
	union all select 'pungent' as name,'p' as [key] 
	union all select 'spicy' as name,'s' as [key]
	)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[5] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[5] not in (select [key] from reference)
	)
insert into Dim_odor
             (
                          odor_Key  
                        , odor_name 
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_odor as d where stub_records.[key] = d.odor_Key);


with reference as 
(	
	select 'abundant' as name,'a' as [key] 
	union all select 'clustered' as name,'c' as [key] 
	union all select 'numerous' as name,'n' as [key] 
	union all select ' scattered' as name,'s' as [key] 
	union all select 'several' as name,'v' as [key] 
	union all select 'solitary' as name,'y' as [key]
	)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[21] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[21] not in (select [key] from reference)
	)
insert into Dim_population
             (
                           population_Key  
                        , population_name 
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_population as d where stub_records.[key] = d.population_Key);


with reference as 
(	
	select 'cobwebby' as name,'c' as [key] 
	union all select 'evanescent' as name,'e' as [key] 
	union all select 'flaring' as name,'f' as [key] 
	union all select 'large' as name,'l' as [key] 
	union all select ' none' as name,'n' as [key] 
	union all select 'pendant' as name,'p' as [key] 
	union all select 'sheathing' as name,'s' as [key] 
	union all select 'zone' as name,'z' as [key]
	)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[19] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[19] not in (select [key] from reference)
	)
insert into Dim_ring_type
             (
                          ring_type_Key  
                        , ring_type_name 
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_ring_type as d where stub_records.[key] = d.ring_type_Key);


with reference as 
(	
	select 'black' as name,'k' as [key] 
	union all select 'brown' as name,'n' as [key] 
	union all select 'buff' as name,'b' as [key] 
	union all select 'chocolate' as name,'h' as [key] 
	union all select 'green' as name,'r' as [key] 
	union all select ' orange' as name,'o' as [key] 
	union all select 'purple' as name,'u' as [key] 
	union all select 'white' as name,'w' as [key] 
	union all select 'yellow' as name,'y' as [key]
	)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[20] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[20] not in (select [key] from reference)
	)
insert into Dim_spore_print_color
             (
                          spore_print_color_Key 
                        , spore_print_color_name
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_spore_print_color as d where stub_records.[key] = d.spore_print_color_Key);


with reference as 
(	
	select 'brown' as name,'n' as [key] 
	union all select 'buff' as name,'b' as [key] 
	union all select 'cinnamon' as name,'c' as [key] 
	union all select 'gray' as name,'g' as [key] 
	union all select 'orange' as name,'o' as [key] 
	union all select ' pink' as name,'p' as [key] 
	union all select 'red' as name,'e' as [key] 
	union all select 'white' as name,'w' as [key] 
	union all select 'yellow' as name,'y' as [key]
	)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[14] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[14] not in (select [key] from reference)
	)
insert into Dim_stalk_color_above_ring
             (
                          stalk_color_above_ring_Key  
                        , stalk_color_above_ring_name 
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_stalk_color_above_ring as d where stub_records.[key] = d.stalk_color_above_ring_Key);


with reference as 
(	
	select 'brown' as name,'n' as [key] 
	union all select 'orange' as name,'o' as [key] 
	union all select 'white' as name,'w' as [key] 
	union all select 'yellow' as name,'y' as [key]
	)
, stub_records as 
	(
	select 
	[key], [name]
	from reference

	union all 
	--records in the data but not in the mapping
	select distinct
		s.[17] as [key],
		null as [name]
	from staging.AgaricusLepiota s
	where s.[17] not in (select [key] from reference)
	)
insert into Dim_veil_color
             (
                           veil_color_Key  
                        , veil_color_name 
             )
select [key], [name]
from stub_records
where not exists (select 1 from Dim_veil_color as d where stub_records.[key] = d.veil_color_Key);

