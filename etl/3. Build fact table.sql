truncate table [dbo].[Fact_AgaricusLepiota];

INSERT INTO [dbo].[Fact_AgaricusLepiota]
           ([cap_shape_ID]
           ,[cap_color_ID]
           ,[odor_ID]
           ,[gill_size_ID]
           ,[gill_color_ID]
           ,[stalk_color_above_ring_ID]
           ,[veil_color_ID]
           ,[ring_type_ID]
           ,[spore_print_color_ID]
           ,[population_ID]
           ,[habitat_ID]
           ,[location_ID]
           ,[Time])
select
[cap_shape_ID]
           ,cc.[cap_color_ID]
           ,o.[odor_ID]
           ,gs.[gill_size_ID]
           ,gc.[gill_color_ID]
           ,scar.[stalk_color_above_ring_ID]
           ,vc.[veil_color_ID]
           ,rt.[ring_type_ID]
           ,spc.[spore_print_color_ID]
           ,p.[population_ID]
           ,h.[habitat_ID]
           ,l.[location_ID]
		   ,Try_parse(s.[time] as time)
from [staging].[AgaricusLepiota] s
inner  join dbo.Dim_location						as l	on l.lat = s.lat and l.lon = s.lon
inner  join dbo.Dim_cap_color					as cc	on s.[3] = cc.cap_color_Key
inner  join dbo.Dim_cap_shape					as cs	on s.[1] = cs.cap_shape_Key	
inner  join dbo.Dim_gill_color					as gc	on s.[9] = gc.gill_color_Key	
inner  join dbo.Dim_gill_size					as gs	on s.[8] = gs.gill_size_Key	
inner  join dbo.Dim_habitat						as h	on s.[22] = h.habitat_Key	
inner  join dbo.Dim_odor							as o	on s.[5] = o.odor_Key	
inner  join dbo.Dim_population					as p	on s.[21] = p.population_Key	
inner  join dbo.Dim_ring_type					as rt	on s.[19] = rt.ring_type_Key	
inner  join dbo.Dim_spore_print_color			as spc	on s.[20] = spc.spore_print_color_Key	
inner  join dbo.Dim_stalk_color_above_ring		as scar	on s.[14] = scar.stalk_color_above_ring_Key	
inner  join dbo.Dim_veil_color					as vc	on s.[17] = vc.veil_color_Key	






