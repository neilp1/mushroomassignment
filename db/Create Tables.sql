
create schema staging


create table staging.AgaricusLepiota
(
[1]		char(1) not null
,[3]	char(1) not null
,[5]	char(1) not null
,[8]	char(1) not null
,[9]	char(1) not null
,[14]	char(1) not null
,[17]	char(1) not null
,[19]	char(1) not null
,[20]	char(1) not null
,[21]	char(1) not null
,[22]	char(1) not null
,lat	DECIMAL(12,9) not null
,lon	DECIMAL(12,9) not null
,[Time] varchar(50) not null

)



create table dbo.Fact_AgaricusLepiota
(
cap_shape_ID				int not null
,cap_color_ID				int not null
,odor_ID					int not null
,gill_size_ID				int not null
,gill_color_ID				int not null
,stalk_color_above_ring_ID	int not null
,veil_color_ID 				int not null
,ring_type_ID				int not null
,spore_print_color_ID		int not null
,population_ID				int not null
,habitat_ID					int not null
,location_ID				int not null
,[Time] time 
)


create table Dim_location
(
location_ID				int identity(1,1) primary key
,lat	DECIMAL(12,9) not null
,lon	DECIMAL(12,9) not null
,location geography not null
)

create table Dim_cap_color
             (
                          cap_color_ID   int identity(1,1) primary key
                        , cap_color_Key  char(1) not null
                        , cap_color_name varchar(50)
             )
create table Dim_cap_shape
             (
                          cap_shape_ID   int identity(1,1) primary key
                        , cap_shape_Key  char(1) not null
                        , cap_shape_name varchar(50)
             )
create table Dim_gill_color
             (
                          gill_color_ID   int identity(1,1) primary key
                        , gill_color_Key  char(1) not null
                        , gill_color_name varchar(50) 
             )
create table Dim_gill_size
             (
                          gill_size_ID   int identity(1,1) primary key
                        , gill_size_Key  char(1) not null
                        , gill_size_name varchar(50) 
             )
create table Dim_habitat
             (
                          habitat_ID   int identity(1,1) primary key
                        , habitat_Key  char(1) not null
                        , habitat_name varchar(50) 
             )
create table Dim_odor
             (
                          odor_ID   int identity(1,1) primary key
                        , odor_Key  char(1) not null
                        , odor_name varchar(50) 
             )
create table Dim_population
             (
                          population_ID   int identity(1,1) primary key
                        , population_Key  char(1) not null
                        , population_name varchar(50) 
             )
create table Dim_ring_type
             (
                          ring_type_ID   int identity(1,1) primary key
                        , ring_type_Key  char(1) not null
                        , ring_type_name varchar(50) 
             )
create table Dim_spore_print_color
             (
                          spore_print_color_ID   int identity(1,1) primary key
                        , spore_print_color_Key  char(1) not null
                        , spore_print_color_name varchar(50) 
             )
create table Dim_stalk_color_above_ring
             (
                          stalk_color_above_ring_ID   int identity(1,1) primary key
                        , stalk_color_above_ring_Key  char(1) not null
                        , stalk_color_above_ring_name varchar(50) 
             )

create table Dim_veil_color
             (
                          veil_color_ID   int identity(1,1) primary key
                        , veil_color_Key  char(1) not null
                        , veil_color_name varchar(50) 
             )