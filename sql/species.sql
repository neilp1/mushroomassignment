
--group by the attributes and then count
with species as 
(
		SELECT [cap_shape_ID]
		  ,[cap_color_ID]
		  ,[odor_ID]
		  ,[gill_size_ID]
		  ,[gill_color_ID]
		  ,[stalk_color_above_ring_ID]
		  ,[veil_color_ID]
		  ,[ring_type_ID]
		  ,[spore_print_color_ID]
	  FROM [dbo].[Fact_AgaricusLepiota]
	  group by [cap_shape_ID]
		  ,[cap_color_ID]
		  ,[odor_ID]
		  ,[gill_size_ID]
		  ,[gill_color_ID]
		  ,[stalk_color_above_ring_ID]
		  ,[veil_color_ID]
		  ,[ring_type_ID]
		  ,[spore_print_color_ID]
)
select count ([cap_shape_ID]) as species_count
from species
