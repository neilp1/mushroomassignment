DECLARE @Origin GEOGRAPHY,
        -- distance defined in meters
        @Distance INTEGER = 10000,
		@lat DECIMAL(12,9)= 37.797500000,
		@lon DECIMAL(12,9)= -105.564300000;        


SET @Origin = geography::Point(@lat, @lon, 4326);

-- 
SELECT distinct cc.[cap_color_name]
FROM [dbo].[Fact_AgaricusLepiota] fal
inner join [dbo].[Dim_cap_color] cc on cc.[cap_color_ID] = fal.[cap_color_ID]
inner join [dbo].[Dim_location] l on l.[location_ID] = fal.[location_ID]
WHERE @Origin.STDistance(l.[location]) <= @Distance;